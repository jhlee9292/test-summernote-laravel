<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Template;

class TemplateController extends Controller {


    public function index() {
        $templates = Template::all();
        return view('template.index', compact('templates'));
    }


    public function create() {
        return view('template.create');
    }


    public function store(Request $request) {

        // set image store path for editor__content images :: WYSIWYG
        $image__storePath = public_path() . '/storage/product-groups';


        /**----------------------------------------------
        // storing WYSIWYG content :: header_template
        ---------------------------------------------- **/
        if( $request->filled('header_template') ) {
            $editor__content__header_template = mb_convert_encoding($request->input('header_template'), 'HTML-ENTITIES', 'UTF-8');

            $dom = new \domdocument();
            $dom->loadHtml($editor__content__header_template, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $dom->encoding = 'utf-8';
            $editor__images = $dom->getelementsbytagname('img');
    


            // loop over img elements, decode their base64 src and save them to public folder,
            // and then replace base64 src with stored image URL.
            foreach($editor__images as $k => $img){
                $data = $img->getattribute('src');
    
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
    
                $data = base64_decode($data);
                $image_name = time().$k.'.png';
                $path = $image__storePath .'/'. $image_name;
    
                // store images 
                file_put_contents($path, $data);

                $img->removeattribute('src');
                $img->setattribute('src', asset('storage/product-groups') .'/'.$image_name);
            }
            $editor__content__header_template = $dom->savehtml();
        } else {
            $editor__content__header_template = null;
        }


        /**----------------------------------------------
        // storing WYSIWYG content :: footer_template
        ---------------------------------------------- **/
        if( $request->filled('footer_template') ) {
            $editor__content__footer_template = mb_convert_encoding($request->input('footer_template'), 'HTML-ENTITIES', 'UTF-8');

            $dom = new \domdocument();
            $dom->loadHtml($editor__content__footer_template, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $dom->encoding = 'utf-8';
            $editor__images = $dom->getelementsbytagname('img');
    

            // loop over img elements, decode their base64 src and save them to public folder,
            // and then replace base64 src with stored image URL.
            foreach($editor__images as $k => $img){
                $data = $img->getattribute('src');
    
                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
    
                $data = base64_decode($data);
                $image_name = time().$k.'.png';
                $path = $image__storePath .'/'. $image_name;
    
                // store images 
                file_put_contents($path, $data);

                $img->removeattribute('src');
                $img->setattribute('src', asset('storage/product-groups') .'/'.$image_name);
            }
            $editor__content__footer_template = $dom->savehtml();
        } else {
            $editor__content__footer_template = null;
        }



        /**----------------------------------------------
        // create new Product Group
        ---------------------------------------------- **/

        $product_group = Template::create([
            'header_template'   => $editor__content__header_template,
            'footer_template'   => $editor__content__footer_template,
        ]);



        /**----------------------------------------------
        // redirect 
        ---------------------------------------------- **/
        return redirect()->route('templates.index');

    }



}
