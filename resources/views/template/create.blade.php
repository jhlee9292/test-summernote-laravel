@extends('layouts.app')

    

@push('header-scripts')
    {{-- Summernote --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
@endpush



@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                

                {{-- Form Here --}}
                <form method="POST" enctype="multipart/form-data" action="{{ route('templates.store') }}">
                    
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="header_template">Header Template</label>
                        <textarea id="header_template" name="header_template" class="form-control" placeholder="Header Template" rows="3"></textarea>
                    </div>


                    <div class="form-group">
                        <label for="footer_template">Footer Template</label>
                        <textarea id="footer_template" name="footer_template" class="form-control" placeholder="Footer Template" rows="3"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary mt-3 mb-0">Create Group</button>
                    
                </form>

            </div>
        </div>
        
    </div>

    
@endsection




@push('footer-scripts')
    
    {{-- SUMMERNOTE --}}
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
        $(document).ready(function() {
            
            $('#header_template').summernote({
                airMode: false,
                toolbar: [
                    ['para', ['ul', 'ol']],
                    ['insert', ['picture'] ],
                ],
                popover: {
                    image: [], link: [], table: [], air: []
                },
            });

            $('#footer_template').summernote({
                airMode: false,
                toolbar: [
                    ['para', ['ul', 'ol']],
                    ['insert', ['picture'] ],
                ],
                popover: {
                    image: [], link: [], table: [], air: []
                },
            });
        });
    </script>
@endpush


