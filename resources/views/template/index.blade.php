@extends('layouts.app')


@section('content')
	

	<div class="container">

		<h1>All Templates Created</h1>
		<a class="btn btn-primary" href="{{ route('templates.create') }}">Create Template</a>

		@foreach( $templates as $template )
			<div class="row mt-5">
				<div class="col">{!! $template->header_template !!}</div>
				<div class="col">{!! $template->footer_template !!}</div>
			</div>
			<hr>
		@endforeach
	</div>


@endsection