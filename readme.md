# Dev Notes

- create new database `test_summernote`
- run `composer install`
- run migration `php artisan migrate`
- run `storage:link` to enable files uploads to public disc
