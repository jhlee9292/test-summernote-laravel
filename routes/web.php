<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/


Route::get('/'					, 'TemplateController@index')->name('templates.index');
Route::get('/template/create'	, 'TemplateController@create')->name('templates.create');
Route::post('/template/store'	, 'TemplateController@store')->name('templates.store');



Auth::routes();